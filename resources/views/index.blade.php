<!doctype html>
	<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<title>{{ config('app.name', 'Laravel') }}</title>
		<link rel="icon" type="image/x-icon" href="/favicon.png">

		<!-- Scripts -->
		<script src="{{ asset('js/app.js') }}" defer></script>

		<!-- Fonts -->
		<link rel="dns-prefetch" href="//fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

		<!-- Icons -->
		<script src="https://use.fontawesome.com/d0f5ac2d3c.js"></script>

		<!-- Styles -->
		<link href="{{ asset('css/app.css') }}" rel="stylesheet"> 
		<link href="{{ asset('css/styles.css') }}" rel="stylesheet">   
		<link href="{{ asset('css/cover.css') }}" rel="stylesheet">   
	</head>
	<body>
		<div id="app">
			<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
				<div class="container">
					<a class="navbar-brand" href="{{ url('/') }}">
						<!-- {{ config('app.name', 'Laravel') }} -->
						<img width="100" src="/1-grid-logo.png" alt="1-grid logo">
					</a>
					<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
						<span class="navbar-toggler-icon"></span>
					</button>

					<div class="collapse navbar-collapse" id="navbarSupportedContent">

						<!-- Right Side Of Navbar -->
						<ul class="navbar-nav ms-auto">

							<!-- Authentication Links -->
							<li class="nav-item">
								<a class="nav-link" href="/blog">Blog</a>
							</li>
							@can(\WebDevEtc\BlogEtc\Gates\GateTypes::MANAGE_BLOG_ADMIN)
							<li class="nav-item">
								<a class="nav-link" href="{{route("blogetc.admin.index")}}">Admin</a>
							</li>
							@endcan
							@guest
							@if (Route::has('login'))
							<li class="nav-item">
								<a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
							</li>
							@endif

							@if (Route::has('register'))
							<li class="nav-item">
								<a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
							</li>
							@endif
							@else
							<li class="nav-item dropdown">
								<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
									{{ Auth::user()->name }}
								</a>

								<div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
									<a class="dropdown-item" href="{{ route('logout') }}"
									onclick="event.preventDefault();
									document.getElementById('logout-form').submit();">
									{{ __('Logout') }}
								</a>

								<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
									@csrf
								</form>
							</div>
						</li>
						@endguest
					</ul>
				</div>
			</div>
		</nav>

		<main class="px-3 text-center">
			<h1>Web Hosting Blog</h1>
			<p class="lead">News for nerds, stuff that matters.</p>
			<p class="lead">
				<a href="/blog" class="btn btn-lg btn-primary ">Continue</a>
			</p>
		</main>
	</div>
</body>
	</html>
