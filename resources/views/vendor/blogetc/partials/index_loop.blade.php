@php
/** @var \WebDevEtc\BlogEtc\Models\Post $post */
@endphp
{{--Used on the index page (so shows a small summary--}}
{{--See the guide on webdevetc.com for how to copy these files to your /resources/views/ directory--}}
{{--https://webdevetc.com/laravel/packages/blogetc-blog-system-for-your-laravel-app/help-documentation/laravel-blog-package-blogetc#guide_to_views--}}

<div class="container">
    <div class="row" style="background-color: #FFF;margin-bottom: 10px;">
            <div style="padding:20px;display:inline-block;">
                {!! $post->imageTag('thumbnail', true, '') !!}<h3 style="display:inline-block;margin-left:20px;"><a href="{{$post->url()}}">{{$post->title}}</a></h3>
                <h5>{{$post->subtitle}}</h5>

                @if(config('blogetc.show_full_post_on_index'))
                {!! $post->renderBody() !!}
                @else
                <p>{!! $post->generateIntroduction(400) !!}</p>
                @endif
                @if($post->posted_at)
    <small>Posted {{ $post->posted_at->diffForHumans() }} @includeWhen($post->author, 'blogetc::partials.author', ['post'=>$post])</small>
@endif

                <div class="text-center">
                    <a href="{{ $post->url() }}" class="btn btn-primary">View Post</a>
                </div>
            </div>
        </div>
    </div>

