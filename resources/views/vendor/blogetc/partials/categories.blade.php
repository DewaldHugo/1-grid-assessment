@php
    /** @var \WebDevEtc\BlogEtc\Models\Post $post */
@endphp
<div><br>
    @foreach($post->categories as $category)
        <a class="btn btn-outline-secondary btn-sm" href="{{ $category->url() }}">
            {{ $category->category_name }}
        </a>
    @endforeach
</div>

