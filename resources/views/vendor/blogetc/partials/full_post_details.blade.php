@php
/** @var \WebDevEtc\BlogEtc\Models\Post $post */
@endphp
@if(\WebDevEtc\BlogEtc\Helpers::hasFlashedMessage())
                        <div class="alert alert-success">
                            <h4>{{\WebDevEtc\BlogEtc\Helpers::pullFlashedMessage() }}</h4>
                        </div>
                    @endif
@can(\WebDevEtc\BlogEtc\Gates\GateTypes::MANAGE_BLOG_ADMIN)
<a href="{{$post->editUrl()}}" class="btn btn-outline-secondary btn-sm pull-right float-right" style="margin-right:8px !important;">Edit Post</a><br><br>
@endcan
<div class="container">
    <div style="background-color: #FFF;padding: 20px;margin-left: -15px !important;margin-right: -15px !important;">
        <h2 class="blog_title text-center">{{$post->title}}</h2>
        <h5 class="blog_subtitle">{{$post->subtitle}}</h5>
        @isset($rating)
        <div class="text-center" style="margin-bottom: 10px;">
                User rating: <img src="/rating{{$rating}}.png" style="max-width:100px;margin-top:-4px;">
        </div>
        @endisset

        {!! $post->imageTag('medium', false, 'd-block mx-auto') !!}

        <p class="blog_body_content">
            {!! $post->renderBody() !!}

            {{--@if(config("blogetc.use_custom_view_files")  && $post->use_view_file)--}}
            {{--                                // use a custom blade file for the output of those blog post--}}
            {{--   @include("blogetc::partials.use_view_file")--}}
            {{--@else--}}
            {{--   {!! $post->post_body !!}        // unsafe, echoing the plain html/js--}}
            {{--   {{ $post->post_body }}          // for safe escaping --}}
            {{--@endif--}}
        </p>

        <hr/>

        @if($post->posted_at)
        Posted {{ $post->posted_at->diffForHumans() }} @includeWhen($post->author, 'blogetc::partials.author', ['post'=>$post])
        @endif


        @includeWhen($post->categories, 'blogetc::partials.categories', ['post'=>$post])
        @can(\WebDevEtc\BlogEtc\Gates\GateTypes::MANAGE_BLOG_ADMIN)
        <hr/>

        <h3>Rate this blog</h3>
        <link rel="stylesheet" href="/css/rating.css">


        <form action="" method="post">
            <div class="stars">  
                <input class="star star-5" id="star-5" type="radio" name="star" value="5" />
                <label class="star star-5" for="star-5"></label>
                <input class="star star-4" id="star-4" type="radio" name="star" value="4" />
                <label class="star star-4" for="star-4"></label>
                <input class="star star-3" id="star-3" type="radio" name="star" value="3" />
                <label class="star star-3" for="star-3"></label>
                <input class="star star-2" id="star-2" type="radio" name="star" value="2" />
                <label class="star star-2" for="star-2"></label>
                <input class="star star-1" id="star-1" type="radio" name="star" value="1" required />
                <label class="star star-1" for="star-1"></label>  
            </div>

            <br>
            <input type="submit" value="Rate !" class="btn btn-primary submitbutton">
            {!! csrf_field() !!}

        </form>
        @endcan
    </div>
</div>

