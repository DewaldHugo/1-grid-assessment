<?php

namespace App\Http\Controllers;

use App\Models\blog_etc_ratings;
use App\Http\Requests\Storeblog_etc_ratingsRequest;
use App\Http\Requests\Updateblog_etc_ratingsRequest;
//use WebDevEtc\BlogEtc\Requests\AddNewblog_etc_ratingsRequest;
use WebDevEtc\BlogEtc\Services\PostsService;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use WebDevEtc\BlogEtc\Helpers;
use Illuminate\Support\Facades\Session;



class BlogEtcRatingsController extends Controller
{
    /** @var PostsService */
    private $postsService;

    public function __construct(
        PostsService $postsService
    ) {
        $this->postsService = $postsService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Storeblog_etc_ratingsRequest $request, $slug)
    {

        //
    }

    public function add(Request $request, $slug)
    {

        ($request);

        $blogPost = $this->postsService->repository()->findBySlug($slug);

        $blog_etc_ratings = new blog_etc_ratings;
 
        $blog_etc_ratings->blog_etc_post_id = $blogPost->id;
        $blog_etc_ratings->user_id = (int) Auth::id();
        $blog_etc_ratings->ip = $request->ip();
        $blog_etc_ratings->rating = $request->post('star');
 
        $blog_etc_ratings->save();

        $rating = \DB::select('select round(sum(rating)/count(*)) as rating from blog_etc_ratings where blog_etc_post_id = '.$blogPost->id);

        Helpers::flashMessage('Thank you for rating this blog!');

        return view(
            'blogetc::single_post',
            [
                'post' => $blogPost,
                'rating' => $rating[0]->rating,
            ]
        );

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Storeblog_etc_ratingsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Storeblog_etc_ratingsRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\blog_etc_ratings  $blog_etc_ratings
     * @return \Illuminate\Http\Response
     */
    public function show(blog_etc_ratings $blog_etc_ratings)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\blog_etc_ratings  $blog_etc_ratings
     * @return \Illuminate\Http\Response
     */
    public function edit(blog_etc_ratings $blog_etc_ratings)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Updateblog_etc_ratingsRequest  $request
     * @param  \App\Models\blog_etc_ratings  $blog_etc_ratings
     * @return \Illuminate\Http\Response
     */
    public function update(Updateblog_etc_ratingsRequest $request, blog_etc_ratings $blog_etc_ratings)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\blog_etc_ratings  $blog_etc_ratings
     * @return \Illuminate\Http\Response
     */
    public function destroy(blog_etc_ratings $blog_etc_ratings)
    {
        //
    }
}
