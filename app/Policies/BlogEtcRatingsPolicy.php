<?php

namespace App\Policies;

use App\Models\User;
use App\Models\blog_etc_ratings;
use Illuminate\Auth\Access\HandlesAuthorization;

class BlogEtcRatingsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\blog_etc_ratings  $blogEtcRatings
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, blog_etc_ratings $blogEtcRatings)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\blog_etc_ratings  $blogEtcRatings
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, blog_etc_ratings $blogEtcRatings)
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\blog_etc_ratings  $blogEtcRatings
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, blog_etc_ratings $blogEtcRatings)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\blog_etc_ratings  $blogEtcRatings
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, blog_etc_ratings $blogEtcRatings)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\blog_etc_ratings  $blogEtcRatings
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, blog_etc_ratings $blogEtcRatings)
    {
        //
    }
}
