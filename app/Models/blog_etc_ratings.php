<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class blog_etc_ratings extends Model
{
    use HasFactory;

    protected $table = 'blog_etc_ratings';

    public $timestamps = true;

    protected $fillable = [
        'blog_etc_post_id',
        'user_id',
        'ip',
        'rating',
    ];
}
